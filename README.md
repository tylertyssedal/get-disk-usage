## Get disk information, including file and folder size. 

1. Run `npm install`

2. Run `npm run start {drivePath}`, such as `npm run start h:/` on Windows to check your H: drive. 

Note: Requires Node 7.6 or higher for async. 
