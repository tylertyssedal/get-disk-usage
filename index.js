var fs = require('fs');
var getSize = require('get-folder-size');
var path = process.argv[2];
 

fs.readdir(path, async function(err, items) {

  var json = {}; 
  var key = 'files';
  json[key] = [];

  for (var i=0; i<items.length; i++) {
    var fqFile = path + items[i];
    var size; 
    var stats = fs.statSync(fqFile);

    if (stats.isDirectory()) {
      let dirSize;
      try {
        dirSize = await asyncGetSize(fqFile);
      } catch (e) {
        console.log(e);
      }
      size = dirSize;
    } else {
      size = stats.size;
    }

    var obj = {};
    obj[fqFile] = size;
    json[key].push(obj);
  }

  console.log(json);
});

function asyncGetSize(fqFile) {
  return new Promise((resolve, reject) => {
    getSize(fqFile, function(err, size) {
      if (err) { 
        return reject(err); 
      }
      return resolve(size);
    });
  });
}